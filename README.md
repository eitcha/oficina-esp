Oficina NodeMCU - Explorando a rede com as coisas
===========================

Oficina da [EITCHA](http://eitcha.org), para aquelas pessoas que querem entender melhor como funcionam as redes de computadores e querem aprender a usá-las para criar projetos.
Previsão de 2h de oficina.

O objetivo desse material é apresentar uma oficina completa e auto-contida, que não precise de muitos outros recursos para funcionar.

### Material necessário ###

* IDE Arduino versão 1.8+ com a placa do NodeMCU adicionada (caso não possua, requer acesso à internet);
* NodeMCU com seu cabo.

Conteúdo
==============


### Parte I ###

* O que é o NodeMCU?
* O que é a tal da Internet das Coisas?
* Primeiros passos com o NodeMCU.

Projetos: Blink, WiFiScan, criando um servidor capaz de atualizar sua página via _serial monitor_.

Saiba mais
============

* Aprenda a controlar o ESP8266 com a IDE do Arduino [aqui](https://github.com/esp8266/Arduino) [em inglês].
* Projeto 1 baseado nos trabalhos do [Leonardo](http://cta.if.ufrgs.br/issues/281) [em português].

